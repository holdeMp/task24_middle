﻿using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
namespace System.Web.Mvc.Html
{
    public static class HelperUI
    {
        public static MvcHtmlString CheckBoxSimple(this HtmlHelper htmlHelper, string name, object htmlAttributes)
        {
            string checkBoxWithHidden = htmlHelper.CheckBox(name, htmlAttributes).ToHtmlString().Trim();
            string pureCheckBox = checkBoxWithHidden.Substring(0, checkBoxWithHidden.IndexOf("<input", 1));
            return new MvcHtmlString(pureCheckBox);
        }
        public static MvcHtmlString MyInlineHelper(this HtmlHelper htmlhelper, string[] words)
        {
            StringBuilder s=new StringBuilder();
            s.Append("<ol>");
            foreach(var word in words)
            {
                if (word == "Email")
                {
                    s.Append("<li>" + word + htmlhelper.TextBox(word,null,new {type="email" }) + "</li>");
                    continue;
                }
                s.Append("<li>"+word + htmlhelper.TextBox(word)+"</li>");
            }
            s.Append("</ol>");
            return new MvcHtmlString(s.ToString());
        }

    }

}