﻿using System.Web.Mvc;
using Task23_Advanced.Models;

namespace Task23_Advanced.Controllers
{
    public class MainController : Controller
    {
        // GET: Main
        public ActionResult Index()
        {
            var db = new LibraryContext();
            return View(db.GetArticles());
        }
    }
}