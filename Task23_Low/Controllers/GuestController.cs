﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23_Advanced.Models;

namespace Task23_Advanced.Controllers
{
    public class GuestController : Controller
    {
        // GET: Guest
        public ActionResult Index()
        {
            var db = new LibraryContext();
            return View(db.GetReviews());
        }
    }
}