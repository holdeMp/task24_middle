﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Task23_Advanced.Models
{
    public class LibraryContext : DbContext
    {
        public LibraryContext() : base("LibraryContext")
        {
        }
        /// <summary>
        /// <c>GetNews()</c> method to get list of news from database
        /// </summary>
        /// <returns><c>List<News></c></returns>
        public List<News> GetNews()
        {
            var db = new LibraryContext();
            var news = db.News.ToList();
            return news;
        }
        /// <summary>
        /// <c>GetReviews()</c> method to get list of reviews from database
        /// </summary>
        /// <returns><c>List<Reviews></c></returns>
        public List<Review> GetReviews()
        {
            var db = new LibraryContext();
            var reviews = db.Reviews.ToList();
            return reviews;
        }
        /// <summary>
        /// <c>GetArticles()</c> method to get list of articles from database
        /// </summary>
        /// <returns><c>List<Articles></c></returns>
        public List<Article> GetArticles()
        {
            var db = new LibraryContext();
            var articles = db.Articles.ToList();
            return articles;
        }
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                string errorMessages = string.Join("; ", ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.PropertyName + ": " + x.ErrorMessage));
                throw new DbEntityValidationException(errorMessages);
            }
        }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<Article> Articles { get; set; }
    }
}